//
//  ViewWyswietlane.swift
//  git-test
//
//  Created by programista on 23.01.2016.
//  Copyright © 2016 programista. All rights reserved.
//

import Foundation
import UIKit


class ViewContWyswietl: UIViewController {
    
    @IBOutlet weak var DoWyswietlenia: UILabel!
    
    var LabelText = String()
    
    @IBOutlet weak var ZmienImage: UIImageView!
    var tapGestureExport = UILongPressGestureRecognizer()
    
    @IBAction func PowrotKlikniety(sender: UIButton) {
        
        let notification:UILocalNotification = UILocalNotification()
        //notification.category = "Wciśniety powrót 😎"
        notification.alertAction = "uśniechnij się 😎" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
        notification.alertBody = " 😎 " + LabelText // text that will be displayed in the notification
        notification.soundName = UILocalNotificationDefaultSoundName // play default sound
        notification.fireDate = NSDate(timeIntervalSinceNow: 5) // 5 sec after fire 
        
        notification.applicationIconBadgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber + 1
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        
        print(" 2a kliknięty powrót 😎")
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        DoWyswietlenia.text = LabelText
     //   DoWyswietlenia.textColor=UIColor.yellowColor()
        
        print( " 2 załadowany 😇")
        
   //ZmienImage.image = ZmienImage.image!.imageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate)
        //     ZmienImage.tintColor = UIColor.blueColor()
        
        ZmienImage.userInteractionEnabled = true
        
        tapGestureExport = UILongPressGestureRecognizer(target: self, action: "tapGestureExportAction:")
        ZmienImage.addGestureRecognizer(tapGestureExport)
        
    }
    
    
    //zmiana sliderem przezroczystości opisu 
    @IBAction func SliderZmiana(sender: UISlider) {
        DoWyswietlenia.alpha=CGFloat(sender.value)
    }
    
    
    
    
    func tapGestureExportAction(sender: UITapGestureRecognizer) {
        
        switch(tapGestureExport.state) {
        case UIGestureRecognizerState.Began,UIGestureRecognizerState.Changed:
        
            let images1: UIImage = UIImage(named: "images1")!
            ZmienImage.image=images1
            
        case UIGestureRecognizerState.Ended:
            let image: UIImage = UIImage(named: "image")!
            ZmienImage.image=image
        default: break // unknown tap
        }
    }
}



