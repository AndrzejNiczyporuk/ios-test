//
//  ViewController.swift
//  git-test
//
//  Created by programista on 21.01.2016.
//  Copyright © 2016 programista. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    
    
    /* wykonanie wpisaywania  liczby zgodnie z linkiem http://www.globalnerdy.com/2015/04/27/how-to-program-an-ios-text-field-that-takes-only-numeric-input-or-specific-characters-with-a-maximum-length/ */
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print (" 1 załadowany 😆")
       initializeTextFields()
        TestIloscSek.text="1"
        TestIloscElem.text="5"
        
    }

    
    @IBOutlet weak var TestPokaz: UITextField!
    @IBOutlet weak var TestIloscSek: UITextField!
    @IBOutlet weak var TestIloscElem: UITextField!

    @IBAction func PokazPressed(sender: AnyObject) {
        print("1a Kliknięty Pokaz 😎")
        TestPokaz.text="Klikniety " + TestPokaz.text!
        self.TestPokaz.resignFirstResponder() // ukrywanie klawiatrury po kliku poza pola
        
    }
    
    func initializeTextFields() {
        
        TestIloscSek.delegate = self
        TestIloscSek.keyboardType = UIKeyboardType.NumberPad
        
        TestIloscElem.delegate = self
        TestIloscElem.keyboardType = UIKeyboardType.NumberPad
    }
    
    // MARK: UITextFieldDelegate events and related methods
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        // We still return true to allow the change to take place.
        if string.characters.count == 0 {
            return true
        }
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        switch textField {
            
            // Allow only digits in this field,
            // and limit its contents to a maximum of 1 characters.
        case TestIloscSek:
            return prospectiveText.containsOnlyCharactersIn("0123456789") &&
                prospectiveText.characters.count <= 1
            
            // Allow only digits in this field,
            // and limit its contents to a maximum of 2 characters.
        case TestIloscElem:
            return prospectiveText.containsOnlyCharactersIn("0123456789") &&
                prospectiveText.characters.count <= 2
            

                        
            // Do not put constraints on any other text field in this view
            // that uses this class as its delegate.
        default:
            return true
        }
        
    }
 
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let button = sender as! UIButton
        
        switch  button.tag {
        case 0:
            let DestViewCont : ViewContWyswietl = segue.destinationViewController as! ViewContWyswietl
            DestViewCont.LabelText = TestPokaz.text!
        case 1:
            let DestViewCont : TableViewController = segue.destinationViewController as! TableViewController
            DestViewCont.LabelText = TestPokaz.text!
            DestViewCont.numElem = Int(TestIloscElem.text!)!
            DestViewCont.numSection = Int(TestIloscSek.text!)!

        default:
            print("default")
        }
       
    }
   
    // ukrywanie klawiatrury po kliku poza pola
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    

}

