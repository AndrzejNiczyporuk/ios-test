//
//  TableViewControler.swift
//  git-test
//
//  Created by programista on 08.02.2016.
//  Copyright © 2016 programista. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDataSource {

    var LabelText = String()
    var numSection = 0
    var numElem = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        print (" 3 załadowany 😆")
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return numSection
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numElem
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        cell.textLabel?.text=LabelText + " \(indexPath.row+1)"
        return cell
    }
    
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "To jest sekcja \(section+1)"
        
    }

    
}
